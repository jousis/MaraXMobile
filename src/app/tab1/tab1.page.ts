import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { BleService, BLE_STATUS, maraXInfoCls } from './../services/ble.service';
import { Component, NgZone } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { runInZone } from '../services/observable-util.service';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

    private destroyed = new Subject<boolean>();
    // connected: boolean = false;
    // connecting: boolean = false;

    bleStatus: BLE_STATUS = BLE_STATUS.BLE_DISCONNECTED;
    bleSignalStrength: number = 100;
    bleError: String = "";
    isLoading: boolean = false;


    currentTimer: String = "";
    smoothTimer: String = "";
    lastTimer: String = "";

    public maraXInfo: maraXInfoCls;
    public hxtemp: string = "";


    // GAUGE_UPD
    public hxGaugeLabelRef: (item, value) => void;
    public sGaugeLabelRef: (item, value) => void;
    public stGaugeLabelRef: (item, value) => void;


    constructor(private ble: BleService,
        private zone: NgZone,
        public loadingController: LoadingController) {


        // GAUGE_UPD
        this.hxGaugeLabelRef = (item, value) => this.gaugeLabel("hx", item, value);
        this.sGaugeLabelRef = (item, value) => this.gaugeLabel("s", item, value);
        this.stGaugeLabelRef = (item, value) => this.gaugeLabel("st", item, value);

    }



    async presentLoading() {
        this.isLoading = true;
        return await this.loadingController.create({
            message: 'Please wait ...',
            spinner: 'circles'
        }).then(a => {
            a.present().then(() => {
                if (!this.isLoading) {
                    a.dismiss().then(() => console.log('dismiss loading'));
                }
            });
        });
    }

    async dismissLoading() {
        this.isLoading = false;
        return await this.loadingController.getTop().then(a => {
            if (a)
                a.dismiss().then(() => console.log('loading dismissed'));
        });
    }

    getBLEStatusClass() {
        if (this.bleSignalStrength < 20) {
            return "ble-error";
        } else if (this.bleSignalStrength < 50) {
            return "ble-warning";
        } else {
            return "";
        }
    }



    //// Change the [label] of the component to gaugeLabelSimple instead of hxGaugeLabelRef,...
    // REGULAR_GAUGE
    // gaugeLabelSimple(value) {
    //     return parseInt(value).toFixed() + "°C";
    // }


    // GAUGE_UPD
    //Uncomment this block and others with GAUGE_UPD and comment the above if you modified the gauge.js
    // https://github.com/naikus/svg-gauge/issues/9
    gaugeLabel(type: string, item: any, value: any): string {
        let label: string = "HX"
        switch (type) {
            case "s":
                label = "Steam"
                break;
            case "st":
                label = "Target"
        }
        while (item.firstChild) {
            item.removeChild(item.firstChild);
        }
        var tspan1 = document.createElementNS("http://www.w3.org/2000/svg", "tspan");
        tspan1.textContent = parseInt(value) + " °C";
        tspan1.setAttribute('x', "50");
        tspan1.setAttribute('y', "48");
        item.appendChild(tspan1);

        var tspan2 = document.createElementNS("http://www.w3.org/2000/svg", "tspan");
        tspan2.textContent = label;
        tspan2.setAttribute('x', "50");
        tspan2.setAttribute('y', "62");
        item.appendChild(tspan2);
        return (label + parseInt(value).toFixed(0) + " C");
    }



    ionViewWillEnter() {
        this.ble.bleStatus.pipe(
            takeUntil(this.destroyed),
            runInZone(this.zone)
        ).subscribe(status => {
            this.bleStatus = status;
            this.bleError = this.ble.bleLastError;
            if (this.bleStatus == BLE_STATUS.BLE_CONNECTED || this.bleStatus == BLE_STATUS.BLE_ERROR) {
                if (this.isLoading) {
                    this.dismissLoading();
                }
            }
        });



        this.ble.maraXInfo.pipe(
            takeUntil(this.destroyed),
            runInZone(this.zone)
        ).subscribe((value: maraXInfoCls) => {
            this.maraXInfo = value;
            this.currentTimer = (value.timer / 1000).toFixed(1);
            this.lastTimer = (value.lastPumpOnDuration / 1000).toFixed(2);
        })
        this.ble.smoothTimer.pipe(
            takeUntil(this.destroyed),
            runInZone(this.zone)
        ).subscribe((value: number) => {
            this.smoothTimer = (value / 1000).toFixed(2);
        })
        this.ble.bleSignalStrength.pipe(
            takeUntil(this.destroyed),
            runInZone(this.zone)
        ).subscribe((value: number) => {
            this.bleSignalStrength = value;
        })
        if (this.bleStatus == BLE_STATUS.BLE_DISCONNECTED) {
            this.autoConnect();
        }
    }



    async autoConnect() {
        this.presentLoading();
        this.ble.autoConnect().then(() => {
            console.debug("autoconnect resolved");
        });
    }

    async connectBLE() {
        this.presentLoading();
        await this.ble.bleScanAndSelect(true).then(result => {
            console.debug("bleScanAndSelect resolved " + result);
        })
    }

    disconnectBLE() {
        this.ble.bleDisconnect();

    }

    hxHistory() {
        this.ble.getHxHistory();
    }

    ionViewDidLeave() {
        this.destroyed.next(true);
    }

}
