import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SettingsService } from './../services/settings.service';
import { BleService, BLE_STATUS } from './../services/ble.service';
import { Component } from '@angular/core';




@Component({
    selector: 'app-tab3',
    templateUrl: 'tab3.page.html',
    styleUrls: ['tab3.page.scss']
})
export class Tab3Page {


    private destroyed = new Subject<boolean>();
    bleStatus: BLE_STATUS = BLE_STATUS.BLE_DISCONNECTED;
    connected: boolean = false;

    wifi: string = "";
    wifipassword: string = "";

    constructor(
        private ble: BleService,
        private settings: SettingsService
    ) {
    }

    ionViewWillEnter() {
        this.ble.bleStatus.pipe(
            takeUntil(this.destroyed)
        ).subscribe(status => {
            this.bleStatus = status;
            if (status == BLE_STATUS.BLE_CONNECTED) {
                this.connected = true;
            }
        });


    }
    saveWifiSettings() {
        this.ble.sendWifiSettings(this.wifi, this.wifipassword);
    }

    ionViewDidLeave() {
        this.destroyed.next(true);
    }
}

