import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { Tab2Page } from './tab2.page';

import { Tab2PageRoutingModule } from './tab2-routing.module';
import { ChartsModule } from 'ng2-charts';

@NgModule({
    imports: [
        SharedModule,
        Tab2PageRoutingModule,
        ChartsModule
    ],
    declarations: [Tab2Page]
})
export class Tab2PageModule { }
