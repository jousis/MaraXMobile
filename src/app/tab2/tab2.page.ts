import { Component, NgZone } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { interval, Subject } from 'rxjs';
import { BleService, BLE_STATUS } from './../services/ble.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { runInZone } from '../services/observable-util.service';



@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

    private destroyed = new Subject<boolean>();
    private stopHistory = new Subject<boolean>();

    private bleStatus: BLE_STATUS = BLE_STATUS.BLE_DISCONNECTED;


    // lineChartData: ChartDataSets[] = [{
    //     data: [1, 50, 97, 44, 95, 89, 90, 91, 92, 93, 94, 95, 1], label: 'HX TEMP'
    // }];
    hxlineChartData: ChartDataSets[] = [{
        data: [], label: 'HX', pointRadius: 0, pointBorderColor: 'red'
    }];
    slineChartData: ChartDataSets[] = [{
        data: [], label: 'Steam', pointRadius: 0, pointBorderColor: 'red'
    }, {
        data: [], label: 'Steam Target', pointRadius: 0, pointBorderColor: 'blue'
    }
    ];

    hxlineChartColors: Color[] = [
        {
            borderColor: '#6f4e37',
            backgroundColor: '#6f4e37',
        },
    ];
    slineChartColors: Color[] = [
        {
            borderColor: '#66c0f4',
            // backgroundColor: 'orange',
        },
        {
            borderColor: '#1b2838',
            // backgroundColor: 'orange',
        },
    ];

    lineChartLabels: Label[];
    lineChartOptions = {
        responsive: true,
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom',
                ticks: {
                    reverse: false,
                }

            }]
        }
    };
    lineChartLegend = true;
    lineChartPlugins = [];
    lineChartType = 'line';

    constructor(private ble: BleService,
        private zone: NgZone) { }

    ionViewWillEnter() {
        this.ble.bleStatus.pipe(
            takeUntil(this.destroyed),
            runInZone(this.zone)
        ).subscribe(status => {
            this.bleStatus = status;
            if (status == BLE_STATUS.BLE_CONNECTED) {
                this.startHistoryMonitor();
            } else {
                this.stopHistoryMonitor();
            }
        });

        this.ble.hxTempHistory.pipe(
            takeUntil(this.destroyed),
            runInZone(this.zone)
        ).subscribe((value: Uint8Array) => {
            let arraydata = Array.from(value).map((d, i) => {
                return { x: -i, y: d }
            })
            this.hxlineChartData[0].data = arraydata;
            this.slineChartData[0].data = arraydata;
            let arraydata2 = Array.from(value).map((d, i) => {
                return { x: -i, y: d + 5 }
            })
            this.slineChartData[1].data = arraydata2;
        })
    }

    startHistoryMonitor() {
        interval(2000)
            .pipe(
                takeUntil(this.stopHistory)
            )
            .subscribe(() => {
                this.ble.getHxHistory();
            })
    }
    stopHistoryMonitor() {
        this.stopHistory.next(true);
    }

    hxHistory() {
        this.ble.getHxHistory();
    }

    ionViewDidLeave() {
        this.destroyed.next(true);
    }
}
