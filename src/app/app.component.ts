import { TranslateService } from '@ngx-translate/core';
import { BleService } from './services/ble.service';
import { SettingsService } from './services/settings.service';
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private settings: SettingsService,
        private ble: BleService,
        private translate: TranslateService,

    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });

        let lang: string = localStorage.getItem('defaultLang') || 'en';
        this.translate.use(lang);
        this.translate.setDefaultLang(lang);


    }
}
