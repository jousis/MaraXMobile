import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    public bleDeviceID: BehaviorSubject<string> = new BehaviorSubject("");

    constructor() {
        this.getDeviceID();
    }


    async getDeviceID() {
        const devid = await Storage.get({ key: 'deviceid' });
        if (devid?.value != null && devid?.value != "null" && devid?.value != "undefined") {
            this.bleDeviceID.next(devid.value);
        }
    }

    async saveDeviceID(deviceid: string) {
        await Storage.set({
            key: "deviceid",
            value: deviceid
        });
        this.bleDeviceID.next(deviceid);
    }

}
