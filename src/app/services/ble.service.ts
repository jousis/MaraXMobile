import { SettingsService } from './settings.service';
import { Injectable, OnDestroy } from '@angular/core';
import { BleClient } from '@capacitor-community/bluetooth-le';
import { Subject, BehaviorSubject, of, interval, Observable } from 'rxjs';
import { map, switchMap, takeUntil, throwIfEmpty } from 'rxjs/operators';



const MARAX_HXTEMP_SERVICE = '46a9920a-b616-44e0-b33e-ac36ab2fe087';


const MARAX_SETTINGS_WRITE = '8bab20c8-903e-4423-a186-b68d2411ac09';

const MARAX_HXTEMP_CHARACTERISTIC_NOTIFY = '1b2c8e21-f323-4b9a-82f7-393b63a5917d';
const MARAX_HXTEMP_HISTORY_READ = 'fdaa14a3-5ab5-42b0-96eb-815c2a57b68b'; //buffer of hx temp
const MARAX_STEAMTEMP_HISTORY_READ = '51412868-e2b6-43dc-a10d-cc5a73c0e566'; //buffer of steam temp
const MARAX_STEAMTARGETTEMP_HISTORY_READ = '0dad858a-033c-46c5-930d-2eac5dd58478';//buffer of steam target temp



export class maraXInfoCls {
    condensedstr: string = ""; //optional
    timer: number = 0;
    lastPumpOnDuration: number = 0;
    wifiIP: string = "";
    heatingEnabled: number = 0;
    heatingBoostValue: number = 0;
    pumpEnabled: number = 0;
    hx: number = null;
    s: number = null;
    st: number = null;
}

export enum BLE_STATUS {
    BLE_DISCONNECTED = 0,
    BLE_CONNECTED = 1,
    BLE_CONNECTING = 2,
    BLE_ERROR = -1
}



@Injectable({
    providedIn: 'root'
})
export class BleService implements OnDestroy {

    public maraXInfo: BehaviorSubject<maraXInfoCls> = new BehaviorSubject({ condensedstr: "", timer: 0, lastPumpOnDuration: 0, wifiIP: "", heatingEnabled: 0, heatingBoostValue: 0, pumpEnabled: 0, hx: 0, s: 0, st: 0 });
    public bleStatus: BehaviorSubject<BLE_STATUS> = new BehaviorSubject(BLE_STATUS.BLE_DISCONNECTED);

    public bleLatency: BehaviorSubject<number> = new BehaviorSubject(0);
    public bleSignalStrength: BehaviorSubject<number> = new BehaviorSubject(100); // <500ms latency is 100%, 5000ms = 10% , >5000 disconnected;

    public bleLastError: String = "";

    private lastBLEUpdate: number = 0;

    public smoothTimer: BehaviorSubject<number> = new BehaviorSubject(0); //since we know how a timer advances, we don't have to wait for BLE packet to advance it. ;)
    // private _timerInterval: Observable<number> = undefined;
    private _stopTimerSmoothing: Subject<boolean> = new Subject();

    public hxTempHistory: BehaviorSubject<Uint8Array> = new BehaviorSubject(new Uint8Array());
    private _waitingForHistory: Boolean = false;


    // public autoConnectBLEFailed: Subject<String> = new Subject();

    _deviceID: string = undefined;
    _deviceFound: boolean = false;
    private destroyed = new Subject<boolean>();

    _counter: number = 0;
    public counter: Subject<number> = new Subject();


    devices: any[];
    hxtemp: any = "EMPTY";

    constructor(
        private settings: SettingsService
    ) {
        settings.bleDeviceID.pipe(
            takeUntil(this.destroyed)
        )
            .subscribe((value) => {
                this._deviceID = value;
            })
        this.bleInit();
        this.bleQualityMonitor();
    }



    startTimerSmoothing() {
        interval(100)
            .pipe(
                takeUntil(this._stopTimerSmoothing)
            )
            .subscribe(() => {
                if (this.bleLatency.value < 1000) {
                    this.smoothTimer.next(this.smoothTimer.value + 100);
                } else {
                    this.smoothTimer.next(this.maraXInfo.value.timer);
                }
            })
    }
    stopTimerSmoothing() {
        this._stopTimerSmoothing.next(true);
    }



    bleQualityMonitor() {
        interval(1000)
            .pipe(
                takeUntil(this.destroyed)
            )
            .subscribe(() => {
                if (this.bleStatus.value == BLE_STATUS.BLE_CONNECTED) {
                    const newtime: number = new Date().getTime();
                    this.bleLatency.next(newtime - this.lastBLEUpdate);
                    let ss = Math.floor(50000 / this.bleLatency.value);
                    if (ss > 100) {
                        ss = 100;
                    } else if (ss <= 10) {
                        ss = 0;
                    }
                    this.bleSignalStrength.next(ss);
                    if (this.bleLatency.value > 5000) {
                        console.debug("possible BLE error");
                        this.bleDisconnect();
                    }
                }
            })
    }



    stringToBytes(input: string): any {
        var array = new Uint8Array(input.length);
        for (var i = 0, l = input.length; i < l; i++) {
            array[i] = input.charCodeAt(i);
        }
        return array.buffer;
    }

    bytesToString(buffer: any): string {
        return String.fromCharCode.apply(null, new Uint8Array(buffer));
    }


    stringToDataView(input: string): DataView {
        let dv: DataView = new DataView(this.stringToBytes(input));
        console.debug(dv);
        return dv;
    }





    parseNotifyMessage(value: DataView) {
        let info: maraXInfoCls = JSON.parse(this.bytesToString(value.buffer));
        if (info?.condensedstr?.length > 0) {
            //override all...       
            // console.debug("we have condensed string : " + info?.condensedstr);
            let newInfo: string[] = info?.condensedstr?.split(',');;
            info.timer = parseInt(newInfo[0]);
            info.lastPumpOnDuration = parseInt(newInfo[1]);
            info.wifiIP = newInfo[2];
            info.pumpEnabled = parseInt(newInfo[3]);
            info.heatingBoostValue = parseInt(newInfo[4]);
            info.pumpEnabled = parseInt(newInfo[5]);
            info.hx = parseInt(newInfo[6]);
            info.s = parseInt(newInfo[7]);
            info.st = parseInt(newInfo[8]);

        }
        this._counter++;
        this.counter.next(this._counter);
        if (info.timer > 0 && this.smoothTimer.value == 0) {
            this.smoothTimer.next(info.timer);
            this.startTimerSmoothing();
        }
        if (info.timer == 0 && this.smoothTimer.value > 0) {
            this.stopTimerSmoothing();
            this.smoothTimer.next(0);
        }
        // console.debug(JSON.stringify(info));
        // this.maraXInfo.next({ timer: temps.timer, lastPumpOnDuration: temps.lastPumpOnDuration, wifiIP: temps.wifiIP, hx: temps.hx, s: temps.s, st: temps.st });
        this.maraXInfo.next(info);
    }



    // https://github.com/capacitor-community/bluetooth-le
    public async bleInit() {
        await BleClient.initialize();
    }



    public async autoConnect() {
        try {
            let deviceID: string = "";
            this._deviceFound = false;
            await BleClient.initialize();

            await BleClient.requestLEScan(
                {
                    name: "Mara X",
                    optionalServices: [MARAX_HXTEMP_SERVICE]
                },
                result => {
                    console.log("received new scan result", JSON.stringify(result));
                    deviceID = result.device.deviceId;
                    this._deviceFound = true;
                    BleClient.stopLEScan();
                    this.bleConnect(deviceID);
                },
            );
            setTimeout(async () => {
                if (!this._deviceFound) {
                    await BleClient.stopLEScan();
                    this.bleLastError = "TIMEOUT_NOT_FOUND";
                    this.bleStatus.next(BLE_STATUS.BLE_ERROR);
                }
            }, 15000);
        } catch (err) {
            console.error(err.message);
            this.bleLastError = err.message;
            this.bleStatus.next(BLE_STATUS.BLE_ERROR);
        }
    }

    public async bleScanAndSelect(saveDeviceID: boolean): Promise<boolean> {
        try {
            const device = await BleClient.requestDevice({
                name: "Mara X",
                optionalServices: [MARAX_HXTEMP_SERVICE]
            });
            if (device?.deviceId?.length > 0) {
                if (saveDeviceID) {
                    this.settings.saveDeviceID(device.deviceId);
                }
                await this.bleConnect(device.deviceId);
                return true;
            }
        } catch (error) {
            console.error("ERROR !!!");
            console.error(error.message);
            this.bleLastError = ""; //not actually an error;
            if (error.message.includes("User cancelled the requestDevice() chooser.")) {
                this.bleLastError = ""; //not actually an error;
            }

            this.bleStatus.next(BLE_STATUS.BLE_ERROR);
            return false;
        }
    }


    public async bleConnect(deviceid: string = this._deviceID) {
        try {
            if (deviceid?.length > 0) {
                this.bleLatency.next(0);
                this.lastBLEUpdate = new Date().getTime();
                await BleClient.connect(deviceid);
                console.debug("connection ok");
                this.bleStatus.next(BLE_STATUS.BLE_CONNECTED);
                await BleClient.startNotifications(
                    deviceid,
                    MARAX_HXTEMP_SERVICE,
                    MARAX_HXTEMP_CHARACTERISTIC_NOTIFY,
                    value => {
                        this.lastBLEUpdate = new Date().getTime();
                        this.parseNotifyMessage(value);
                    },
                );
            } else {
                this.bleLastError = "Device ID not found";
                this.bleStatus.next(BLE_STATUS.BLE_ERROR);
            }
        } catch (error) {
            console.error(error);
            this.bleStatus.next(BLE_STATUS.BLE_ERROR);
        }
    }

    public async bleDisconnect(deviceid: string = this._deviceID) {
        try {
            if (deviceid?.length > 0) {
                // this.lastBLEUpdate = new Date().getTime();
                // this.bleLatency.next(0);
                await BleClient.stopNotifications(deviceid, MARAX_HXTEMP_SERVICE, MARAX_HXTEMP_CHARACTERISTIC_NOTIFY);
                await BleClient.disconnect(deviceid);
                this.bleStatus.next(BLE_STATUS.BLE_DISCONNECTED);
            }
        } catch (error) {
            // console.error(error);
            this.bleLastError = error.message;
            this.bleStatus.next(BLE_STATUS.BLE_ERROR);
        }
    }

    public getHxHistory() {
        if (this.bleStatus.value == BLE_STATUS.BLE_CONNECTED) {
            let result: DataView;
            if (!this._waitingForHistory) {
                this._waitingForHistory = true;
                BleClient.read(this._deviceID, MARAX_HXTEMP_SERVICE, MARAX_HXTEMP_HISTORY_READ).then(dv => {
                    this.hxTempHistory.next(new Uint8Array(dv.buffer));
                    this._waitingForHistory = false;
                });
            }

        }
    }

    private handleGATTError(errorMessage: string) {
        if (errorMessage.includes("GATT Server is disconnected")) {
            this.bleDisconnect();
        } else if (errorMessage.includes("Device not found.")) {
            this.bleDisconnect();
        }
    }

    public sendWifiSettings(wifiname: string, wifipassword: string) {
        if (this.bleStatus.value == BLE_STATUS.BLE_CONNECTED) {
            let settings: string = wifiname + ";" + wifipassword;
            BleClient.write(this._deviceID, MARAX_HXTEMP_SERVICE, MARAX_SETTINGS_WRITE, this.stringToDataView(settings))
                .then(dv => {
                    console.debug(dv);
                })
                .catch(error => {
                    this.bleLastError = error.message;
                    this.handleGATTError(error.message)
                })
        }
    }



    ngOnDestroy(): void {
        this.destroyed.next(true);
    }

}
