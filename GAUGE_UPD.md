# Small update for gauge to support 2-line label
[Solution and code from this issue](https://github.com/naikus/svg-gauge/issues/9)  

<br/><br/>
   
# Procedure to enable
**by default, code is compatible but you need to edit gauge.js in order to make it work**  
<br/><br/>
- Install library and other dependencies as usual (`npm i`)
- Edit `node_modules\svg-gauge\src\gauge.js` and change `updateGauge` function (see next section for full final code)
- Edit `tab1.page.html` and change the label property of the 3 gauges
    - `[label]="hxGaugeLabelRef"`  for HX
    - `[label]="sGaugeLabelRef"` for Steam
    - `[label]="stGaugeLabelRef"` for Steam Target
- Edit `tab1.page.ts` and comment the function `gaugeLabelSimple()`  
- Uncomment every block with `GAUGE_UPD`:
    - 3 function definitions (`hxGaugeLabelRef`,`sGaugeLabelRef`,`stGaugeLabelRef`)
    - 3 assignments in the constructor
    - the `gaugeLabel` function


   <br/><br/>
# function updateGauge
````js
            function updateGauge(theValue, frame) {
                var val = getValueInPercentage(theValue, min, limit),
                    // angle = getAngle(val, 360 - Math.abs(endAngle - startAngle)),
                    angle = getAngle(val, 360 - Math.abs(startAngle - endAngle)),
                    // this is because we are using arc greater than 180deg
                    flag = angle <= 180 ? 0 : 1;
                if (displayValue) {
                    if (label.length === 1) {
                        gaugeValueElem.textContent = label.call(opts, theValue);
                    } else {
                        label.call(opts, gaugeValueElem, theValue);
                    }
                }
                gaugeValuePath.setAttribute("d", pathString(radius, startAngle, angle + startAngle, flag));
            }

