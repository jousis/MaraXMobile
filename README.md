# Companion App for Mara X Visualizer
[See the main repo for more info](https://gitlab.com/jousis/marax_visualizer)  
<img src="screenshots/Screenshot_20210123-210733_MaraXMobile.jpg" width="200">
<img src="screenshots/Screenshot_20210124-023505_MaraXMobile.jpg" width="200">
   <img src="screenshots/chrome_debug_galaxyS5.jpg" width="200">
<br/><br/>


# Features
- HX , Steam and Steam target temperature display (2Hz)
- Current timer with fake updates(100ms) between the real updates (every ~500ms)
- BLE signal quality based on latency of received updates
- Autoconnect BLE on open if supported (android and I guess IOS)
- Manual connection on PWA
   
# Todo
- Graphs (2nd tab) are unfinished
- Pump on/off, heating on/off, boost are not displayed

# Tech Details
- Ionic 5 (Angular 10)
- Capacitor 2 (for web/android/ios BLE plugin)
- Plugins:
    - [@capacitor-community/bluetooth-le](https://github.com/capacitor-community/bluetooth-le)
- Libraries:
    - [angular gauge](https://github.com/mattlewis92/angular-gauge)


   <br/><br/>
# Installation - APK
I have built an unsigned APK for your convinience. However I suggest you clone the repo and build your own.  
[Download the -unsigned- APK here](apk/app-debug.apk)
   <br/><br/>
# Installation
- Install the Ionic CLI: `npm install -g @ionic/cli`
- Clone this repository: `git clone https://gitlab.com/jousis/MaraXMobile.git`
- Navigate to repo in a terminal: `cd MaraXMobile`
- Install dependencies: `npm i`
- Check that you have all the [dependencies for Capacitor](https://capacitorjs.com/docs/getting-started/dependencies)
- Update your gauge.js so you can have 2 lines in the label  
    [See GAUGE_UPD.md for more details](./GAUGE_UPD.md)
- Test 
    - locally (BLE works fine in chrome): 
        - `ionic serve`
    - on android device: 
        - `ionic build`  
        - `npx cap sync`  
        - `npx cap open android`
    - ...
